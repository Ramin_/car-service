import {
  Connection,
  Date,
  Document,
  Model,
  Schema,
  SchemaTypes,
} from 'mongoose';
import { User } from './user.model';

interface Timeslot extends Document {
  readonly day: Date;
  readonly startAt: Date;
  readonly endAt: Date;
  readonly isFree: boolean;
  readonly branch: string;
  readonly createdBy?: Partial<User>;
  readonly updatedBy?: Partial<User>;
}

type TimeslotModel = Model<Timeslot>;

const TimeslotSchema = new Schema<Timeslot>(
  {
    day: SchemaTypes.Date,
    startAt: SchemaTypes.Date,
    endAt: SchemaTypes.Date,
    isFree: SchemaTypes.Boolean,
    branch: SchemaTypes.String,
    createdBy: { type: SchemaTypes.ObjectId, ref: 'User', required: false },
    updatedBy: { type: SchemaTypes.ObjectId, ref: 'User', required: false },
  },
  { timestamps: true },
);

const createTimeslotModel: (conn: Connection) => TimeslotModel = (
  conn: Connection,
) => conn.model<Timeslot>('Timeslot', TimeslotSchema, 'timeslots');

export { Timeslot, TimeslotModel, createTimeslotModel };
