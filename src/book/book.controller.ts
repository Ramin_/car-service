import { Body, Controller, Post, Res, UseGuards } from '@nestjs/common';
import { Observable, map } from 'rxjs';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';
import { BookDto } from './book.dto';
import { BookService } from './book.service';

@Controller('book')
export class BookController {
  constructor(private bookService: BookService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  Booked(@Body() book: BookDto, @Res() res: Response): Observable<Response> {
    return this.bookService.save(book).pipe(
      map(() => {
        return res;
      }),
    );
  }
}
