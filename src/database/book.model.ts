import { Connection, Document, Model, Schema, SchemaTypes } from 'mongoose';
import { BookStatus } from 'src/shared/enum/book.status.enum';
import { User } from './user.model';

interface Book extends Document {
  readonly customer: Partial<User>;
  readonly technicians: Partial<User>[];
  readonly status: BookStatus;
  readonly numberplate: string;
  readonly bookAt: Date;
}

type BookModel = Model<Book>;

const BookSchema = new Schema<Book>(
  {
    customer: { type: SchemaTypes.ObjectId, ref: 'User' },
    technicians: [{ type: SchemaTypes.ObjectId, ref: 'User' }],
    status: [
      { type: SchemaTypes.String, enum: ['RESERVED', 'INREPEAIR', 'REPEARD'] },
    ],
    bookAt: SchemaTypes.Date,
    numberplate: SchemaTypes.String,
  },
  { timestamps: true },
);

const createBookModel: (conn: Connection) => BookModel = (conn: Connection) =>
  conn.model<Book>('Book', BookSchema, 'books');

export { Book, BookModel, createBookModel };
