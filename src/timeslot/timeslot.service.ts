import { Inject, Injectable, Scope } from '@nestjs/common';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';
import { TIMESLOT_MODEL } from 'src/database/database.constants';
import { Timeslot } from 'src/database/timeslot.model';
import { CreateTimeslotDto } from './create.timeslot.dto';
import { REQUEST } from '@nestjs/core';

@Injectable({ scope: Scope.REQUEST })
export class TimeslotService {
  constructor(
    @Inject(TIMESLOT_MODEL) private timeslotModel: Model<Timeslot>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) {}

  timeslotByDay(day: string): Observable<Timeslot> {
    return from(this.timeslotModel.findOne({ day: day }).exec());
  }

  isFree(day: string, startAt: string, branch: string): Observable<Timeslot> {
    return from(
      this.timeslotModel
        .findOne({ day: day, startAt: startAt, branch: branch })
        .exec(),
    );
  }

  save(data: CreateTimeslotDto): Observable<Timeslot> {
    if (this.isFree(data.day, data.startAt, data.branch)) {
      return this.isFree(data.day, data.startAt, data.branch);
    }
    const createtimeslot: Promise<Timeslot> = this.timeslotModel.create({
      ...data,
      createdBy: { _id: this.req.user.id },
    });
    return from(createtimeslot);
  }
}
