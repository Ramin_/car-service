import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Post,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { map, Observable } from 'rxjs';
import { HasRoles } from 'src/auth/guard/has-roles.decorator';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guard/roles.guard';
import { RoleType } from 'src/shared/enum/role-type.enum';
import { CreateTimeslotDto } from './create.timeslot.dto';
import { TimeslotService } from './timeslot.service';

@Controller('timeslot')
export class TimeslotController {
  constructor(private timeslotService: TimeslotService) {}

  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HasRoles(RoleType.ADMIN)
  createPost(
    @Body() timeslot: CreateTimeslotDto,
    @Res() res: Response,
  ): Observable<Response> {
    return this.timeslotService.save(timeslot).pipe(
      map(() => {
        return res;
      }),
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async getAllTimeslot(@Query() day: string) {
    const timeslot = await this.timeslotService.timeslotByDay(day);
    if (!timeslot) {
      throw new NotFoundException(`No timeslop for this day: ${day}`);
    }
    return timeslot;
  }
}
