import {
  Controller,
  DefaultValuePipe,
  Get,
  ParseIntPipe,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { BookService } from 'src/book/book.service';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';

@Controller()
export class ProfileController {
  constructor(private bookService: BookService) {}
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Req() req: Request): any {
    return req.user;
  }

  @UseGuards(JwtAuthGuard)
  @Get('books')
  getBookedInformation(
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit?: number,
    @Query('skip', new DefaultValuePipe(0), ParseIntPipe) skip?: number,
  ): any {
    return this.bookService.findAll(skip, limit);
  }
}
