import { Inject, Injectable, NotAcceptableException } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { AuthenticatedRequest } from 'src/auth/interface/authenticated-request.interface';
import { Book } from 'src/database/book.model';
import { BOOK_MODEL } from 'src/database/database.constants';
import { BookStatus } from 'src/shared/enum/book.status.enum';
import { BookDto } from './book.dto';

@Injectable()
export class BookService {
  constructor(
    @Inject(BOOK_MODEL) private bookModel: Model<Book>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) {}
  save(data: BookDto): Observable<Book> {
    if (this.hasReserved(data.numberplate)) {
      throw new NotAcceptableException('');
    }
    const createPost: Promise<Book> = this.bookModel.create({
      ...data,
      status: BookStatus.RESERVED,
      customer: { _id: this.req.user.id },
    });
    return from(createPost);
  }
  async hasReserved(numberplate: string): Promise<number> {
    const startDay = new Date();
    startDay.setUTCHours(0, 0, 0, 0);
    const booked = await this.bookModel
      .find({ numberplate: numberplate, bookAt: { $gte: startDay } })
      .count()
      .exec();
    return booked;
  }

  findAll(skip = 0, limit = 10): Promise<Book[]> {
    return this.bookModel
      .find({
        customer: this.req.user.id,
      })
      .skip(skip)
      .limit(limit)
      .exec();
  }
}
