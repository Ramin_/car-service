import { Module } from '@nestjs/common';
import { TimeslotService } from './timeslot.service';
import { TimeslotController } from './timeslot.controller';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  providers: [TimeslotService],
  exports: [TimeslotService],
  controllers: [TimeslotController],
})
export class TimeslotModule {}
