import { IsDate, IsNotEmpty } from 'class-validator';
export class BookDto {
  @IsDate()
  readonly date: string;

  @IsNotEmpty()
  readonly numberplate: string;
}
