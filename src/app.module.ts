import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './user/user.module';
import { TimeslotModule } from './timeslot/timeslot.module';
import { BookModule } from './book/book.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    // MongooseModule.forRoot(process.env.MONGODB_URI),
    DatabaseModule,
    AuthModule,
    UserModule,
    TimeslotModule,
    BookModule,
  ],
})
export class AppModule {}
