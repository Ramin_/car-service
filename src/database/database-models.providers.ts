import { Connection } from 'mongoose';
import { createBookModel } from './book.model';
import {
  BOOK_MODEL,
  DATABASE_CONNECTION,
  TIMESLOT_MODEL,
  USER_MODEL,
} from './database.constants';
import { createTimeslotModel } from './timeslot.model';
import { createUserModel } from './user.model';

export const databaseModelsProviders = [
  {
    provide: USER_MODEL,
    useFactory: (connection: Connection) => createUserModel(connection),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: TIMESLOT_MODEL,
    useFactory: (connection: Connection) => createTimeslotModel(connection),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: BOOK_MODEL,
    useFactory: (connection: Connection) => createBookModel(connection),
    inject: [DATABASE_CONNECTION],
  },
];
