import { IsDate, IsNotEmpty } from 'class-validator';
export class CreateTimeslotDto {
  @IsDate()
  readonly day: string;

  @IsDate()
  readonly startAt: string;

  @IsDate()
  readonly endAt: string;

  @IsNotEmpty()
  readonly branch: string;
}
